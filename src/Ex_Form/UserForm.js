import { nanoid } from "nanoid";
import React, { Component } from "react";


export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  state = {
    user: {
      name: "",
      address: "",
      email: "",
      phone: "",
    },
  };
  componentDidMount() {
    
  }
  handleUserSubmit = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid(3);
    this.props.handleUserAdd(newUser);
  };

  handleGetUserForm = (e) => {
    console.log(e.target.name);
    let { value, name: key } = e.target;
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };
  componentWillReceiveProps(nextProps) {
    if (nextProps.userEdited != null) {
      this.setState({ user: nextProps.userEdited });
    }
  }
  handleUpdateInfo = () => {
    let userUpdate = { ...this.state.user };
    this.props.handleUpdateUser(userUpdate);
  }

  render() {
    return (
      <div>
        <div>
          <h2 className="text-left bg-dark text-white p-2">Thêm ngươi dùng</h2>
        </div>
        <form>
          <div className="form-group">
            <input
              onChange={this.handleGetUserForm}
              value={this.state.user.name}
              ref={this.inputRef}
              type="text"
              className="form-control"
              name="name"
              placeholder="Name"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleGetUserForm}
              value={this.state.user.address}
              type="text"
              className="form-control"
              name="address"
              placeholder="Address"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleGetUserForm}
              value={this.state.user.email}
              ref={this.inputRef}
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleGetUserForm}
              value={this.state.user.phone}
              ref={this.inputRef}
              type="text"
              className="form-control"
              name="phone"
              placeholder="Phone"
            />
          </div>
        </form>
        <button onClick={this.handleUserSubmit} className="btn btn-success mr-5">Add user</button>
        <button onClick={this.handleUpdateInfo} className="btn btn-primary">Update</button> 
      </div>
    );
  }
}

