import React, { Component } from "react";

export default class UserTable extends Component {
  renderUserTable = () => {
    return this.props.userList.map((item, key) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.address}</td>
          <td>{item.email}</td>
          <td>{item.phone}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleUserRemove(item.id);
              }}
              className="btn btn-danger mr-2"
            >
              Delete
            </button>
            <button
              onClick={() => {
                this.props.handleEditUser(item);
              }}
              className="btn btn-warning"
            >
              Edit
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="text-left mt-5">
        <h2 className="bg-dark text-white p-2">Danh sách người dùng</h2>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{this.renderUserTable()}</tbody>
      </table>
      </div>
    );
  }
}


